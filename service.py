# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np 
import io
import requests
import time
import datetime
import sqlalchemy as sql
import re
import boto3
import os



# Functions
    
def get_article_id(url):
    if 'rtbf.be/auvio/' in path:
        return np.nan
    match = re.search('\?id=[0-9]+($|&)', path)
    if match is not None:
        articleid = path[match.start()+4: match.end()]
        if articleid[-1] == '&':
            return articleid[:-1]
        else:
            return articleid
    else:
        np.nan


def get_query_id(**params):
    response = requests.get('https://api.chartbeat.com/query/v2/submit/page/?apikey={}&endpoint={}&host={}&email={}&user_id={}&limit={}&start={}&end={}&tz={}&dimensions={}&metrics={}&{}={}'.format(params['api_key'], params['endpoint'], params['host'], params['email'], params['user_id'], params['limit'], params['start_date'], params['end_date'], params['tz'], params['dimensions'], params['metrics'], params['filter_key'], params['filter_value'])).json()
    return response['query_id'] if 'query_id' in response else 'Error'


def get_query_status(query_id, **config):
    response = requests.get('https://api.chartbeat.com/query/v2/status/?apikey={}&host={}&query_id={}'.format(config['api_key'], config['host'], query_id)).json()
    return response['status'] if 'status' in response else 'Error'


def ask_for_data(query_id, max_requests=10, **config):
    status = get_query_status(query_id, **config)
    if (status in ['completed', 'downloaded']):
        return status
    elif max_requests > 0:
        time.sleep(10)
        status = ask_for_data(query_id, max_requests-1, **config)
    else:
        return 'Service down'


def get_data(query_id, df_metadata, **config):
    response = requests.get('https://api.chartbeat.com/query/v2/fetch/?apikey={}&host={}&query_id={}'.format(config['api_key'], config['host'], query_id)).text
    df = pd.read_csv(io.StringIO(response))

    if len(df) >= config['limit']:
        print(date_str)

    df['articleid'] = df['path'].apply(get_article_id)
    df = df[(~df['articleid'].isna()) & ~(df['articleid'].str.len() == 0)]

    df = df.rename(columns={'tz_time':'datetime','page_total_time':'total_time'})

    df = df.merge(df_metadata, left_on='articleid', right_on='mediaid', how='left')

    df['articleid'] = df['articleid'].astype(int)

    return df[['datetime', 'articleid', 'nb_words', 'total_time', 'page_views', 'path']]


def handler(event, context):

    # Configuration and constants

    ssm = boto3.client('ssm')
    # redshift_secret = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
    # chartbeat_api_key = ssm.get_parameter(Name='MEIV_CHARTBEAT_API_KEY', WithDecryption=True)

    engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ['db_username'], os.environ['db_pass'], os.environ['db_host'], os.environ['db_port'], os.environ['db_database']))
    # engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ['db_username'], redshift_secret['Parameter']['Value'], os.environ['db_host'], os.environ['db_port'], os.environ['db_database']))

    config = {
        'endpoint': os.environ['cf_endpoint'],
        'host': os.environ['cf_host'],
        'email': os.environ['cf_email'],
        'user_id': os.environ['cf_user_id'],
        'api_key': os.environ['cf_api_key']#chartbeat_api_key['Parameter']['Value']
    }

    query_params = config
    
    query_params.update(event)

    yesterday = datetime.datetime.now().date() - datetime.timedelta(days=1)
    date_str = datetime.datetime.strftime(yesterday, '%Y-%m-%d')

    df_metadata = pd.read_sql_query("""SELECT CAST(mediaid AS varchar), wordcount AS nb_words FROM articles_characters.articles WHERE wordcount IS NOT NULL AND wordcount > 0""", engine)

    query_params.update({'start_date': date_str, 'end_date': date_str})

    query_id = get_query_id(**query_params)

    if query_id != 'Error':
        status = ask_for_data(query_id, **config)
        if status != 'Service down':
            df = get_data(query_id, df_metadata, **config)
            with engine.connect() as connection:
                with connection.begin():
                    for i,row in df.iterrows():
                        query = """INSERT INTO article_metrics VALUES ({},{})""".format("""'{}','{}','{}','{}','{}'""".format(row['datetime'],row['articleid'],row['nb_words'],row['total_time'],row['page_views']).replace("''",'NULL'),"""'{}'""".format(row['path'].replace("'","''")))
                        try:
                            connection.execute(query)
                        except Exception as e:
                            print('Error inserting in article_metrics {}\n{}'.format(query, str(e)))

        else:
            print(status)
    else:
        print(query_id)